/***
 * This project is for a CSE271 lab where the goal is to practice the usage of
 * Git and GitLab.
 * 
 * @author Gunnar Von Bergen
 *
 */
public class Recursion {
    /**
     * A simple recursive power method.
     * 
     * @param base the base number for the exponential calculation.
     * @param n    the power to raise the base to.
     * @return an integer result of the base to the nth power.
     */
    public static int powerN(int base, int n) {
        // Base case
        if (n <= 0) {
            return 1;
        }
        // Call recursively with n - 1
        return base * powerN(base, n - 1);
    }

    /**
     * A simple method that calculates the number of units in a triangle where
     * each row of a triangle has another unit. E.x. row 1 = 1 unit, row 2 = 2
     * blocks...
     * 
     * @param row the number of total rows in the triangle.
     * @return an integer representing the total number of units in all rows.
     */
    public static int triangle(int row) {
        // Base case
        if (row <= 0) {
            return 0;
        }
        // Call recursively with row - 1
        return row + triangle(row - 1);
    }
}
